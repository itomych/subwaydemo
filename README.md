# Subway Orders Demo

Demo task to show basic capabilities of building Web-applications using Laravel+VueJs

## Installation

Clone repository/download sources. Enter sources folder. Build docker container.

```bash
docker-compose build
``` 
Run 
```bash
docker-compose up
```

Enter docker container:
```bash
docker exec -it sandwich_order_php bash
```

Inside container execute:
```bash
composer install
apk add --no-cache npm
npm install
npm run prod
php artisan  migrate
php artisan  db:seed
```

Rename .env.example to .env.

Application should be available by http://localhost:8000

## Contents
Application consists of 2 sections:

* Admin pannel
* User interface

Admin credetials:
```
admin@test.dev
secret
```

Admin functions include:

* Authorization by email/password
* CRUD for Breads/Extras/Tastes/Vegetables/Sources
* Orders management
* User management

User 
* Authorization using specified by admin link 
* User creates order, selecting options for orders (implemented as Wizard steps)
* For new order, previous options are preset (if were filled before)
* Sees his previous orders, can rate it

If you want to test  user functionality, please copy link and logout from admin account, then use link to relogin
or use another browser to login as user!

For example, user Alex:
https://subway.sirius-srv.net/?code=uCLcC2MWthexWCyH8xhkVZIWKrTvT43P 

## Technical details Contributing

Application is written using PHP framework Laravel. 
It is implemented as Single Page Application(SPA) using VueJs frontend framework. Vuetify Material UI framework is used for nice look and feel.
SPA (VueJs) allows user to interact with web-site dynamically rewriting the current page, loading information by AJAX request,
rather than loading entire new page from a server.

MySQL is used to keep the data.

```
www/database/migrations - creates the structure of DB
www/database/seeds - populates DB with needed records
```

Laravel serves as backend api for VueJs application. Returning needed data in JSON format.

```
www/routes/api.php - requests that are handled by backend.
```

VueJs implementation can be found in 
```
www/resources/js/components
```

Vuex(Store) component is used to keep the data on the browser side
```
www/resources/js/components/store
```

Services 

```
www/app/Services/ 
```

implement specific functionality for
```
User authorization, roles, etc.
Order operations 
Options operations
Codes generation
```

Requests - validations of data coming from user to api 

```
www/app/Request/ 
```


Models - User, Role, Order, Option, OptionVariation entities described

```
www/app/*.php
```

Controllers are what handles each http request from frontend part

```
www/app/Http/Controllers 
```


Basic tests are run using command
```
php artisan dusk
```
Test check UserLogin functionality and Loading of Meal Options Pages.

Located in

```
www/tests
```

## Further improvements
* Allow admin to close Meal for Today
* Allow users edit created (non-closed) orders
* Add GPS location detection of user
* In admin section show user locations on the map
* Add button to call Subway using MessageBird and inform about big order

* Acceptance tests to verify business logic is correct:
for example, test that only 1 open meal of a day can be created by admin, test that users can place orders only on opened meals, etc. 


## Web-site
[Subway demo](https://subway.sirius-srv.net/)


