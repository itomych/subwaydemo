<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionVariationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_variations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('option_id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('available');
            $table->timestamps();

            $table
                ->foreign('option_id')
                ->references('id')->on('options')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('option_variations');
    }
}
