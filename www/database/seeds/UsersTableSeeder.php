<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::query()->where('name', env('ROLE_ADMIN'))->first();
        $user = User::create([
            'name' => 'Admin Admin',
            'email' => 'admin@test.dev',
            'password' => bcrypt('secret')
        ]);

        $user->roles()->save($role)->make();
    }
}
