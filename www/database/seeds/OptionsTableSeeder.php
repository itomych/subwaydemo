<?php

use App\Option;
use App\OptionVariation;
use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payload = collect([
            [
                'data' => [
                    'name' => 'Bread',
                    'slug' => 'bread'
                ],

                'variations' => [
                    [
                        'name' => 'Dark bread',
                        'slug' => 'dark_bread',
                        'available' => true
                    ],
                    [
                        'name' => 'Whole grain',
                        'slug' => 'whole_grain_bread',
                        'available' => true
                    ],
                    [
                        'name' => 'Rye bread',
                        'slug' => 'rye_bread',
                        'available' => true
                    ]
                ]
            ],
            [
                'data' => [
                    'name' => 'Bread Size',
                    'slug' => 'bread_size'
                ],

                'variations' => [
                    [
                        'name' => '15 cm',
                        'slug' => '15_cm_bread',
                        'available' => true
                    ],
                    [
                        'name' => '30 cm',
                        'slug' => '30_cm_bread',
                        'available' => true
                    ]
                ]
            ],
            [
                'data' => [
                    'name' => 'Extra',
                    'slug' => 'extra'
                ],

                'variations' => [
                    [
                        'name' => 'Bacon',
                        'slug' => 'extra_bacon',
                        'available' => true
                    ],
                    [
                        'name' => 'Cheese',
                        'slug' => 'extra_cheese',
                        'available' => true
                    ],
                    [
                        'name' => 'Meat',
                        'slug' => 'extra_meat',
                        'available' => true
                    ]
                ]
            ],
            [
                'data' => [
                    'name' => 'Taste',
                    'slug' => 'taste'
                ],
                'variations' => [
                    [
                        'name' => 'Chicken',
                        'slug' => 'chicken_taste',
                        'available' => true
                    ],
                    [
                        'name' => 'Beaf',
                        'slug' => 'beef_taste',
                        'available' => true
                    ],
                    [
                        'name' => 'Pork',
                        'slug' => 'pork_taste',
                        'available' => true
                    ]
                ]
            ],
            [
                'data' => [
                    'name' => 'Vegetable',
                    'slug' => 'vegetable'
                ],
                'variations' => [
                    [
                        'name' => 'Tomato',
                        'slug' => 'tomato_vegetable',
                        'available' => true
                    ],
                    [
                        'name' => 'Carrot',
                        'slug' => 'carrot_vegetable',
                        'available' => true
                    ],
                    [
                        'name' => 'Salad',
                        'slug' => 'salad_vegetable',
                        'available' => true
                    ]
                ]
            ],
            [
                'data' => [
                    'name' => 'Sauce',
                    'slug' => 'sauce'
                ],
                'variations' => [
                    [
                        'name' => 'Jamaican Curried Mayo Sauce',
                        'slug' => 'jamaican_curried_mayo_sauce',
                        'available' => true
                    ],
                    [
                        'name' => 'Pineapple-Lemongrass Chutney Sauce',
                        'slug' => 'pineapple_lemongrass_chutney_sauce',
                        'available' => true
                    ],
                    [
                        'name' => 'Greek Feta-Herb Sauce',
                        'slug' => 'greek_feta_herb_sauce',
                        'available' => true
                    ]
                ]
            ],
        ]);

        $payload->each(function ($item) {
            $option = Option::query()->create($item['data']);

            array_walk($item['variations'], function ($variation) use ($option) {
                $optionVariation = OptionVariation::query()->make($variation);
                $option->variations()->save($optionVariation)->make();
            });
        });
    }
}
