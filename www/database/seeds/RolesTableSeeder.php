<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::query()->create([
            'name' => env('ROLE_ADMIN'),
            'description' => 'Admin User role'
        ]);

        Role::query()->create([
            'name' => env('ROLE_USER'),
            'description' => 'Simple User role'
        ]);
    }
}
