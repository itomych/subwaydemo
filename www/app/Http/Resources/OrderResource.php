<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user,
            'status' => $this->status,
            'should_be_baked' => $this->should_be_baked,
            'rate'=> $this->rate,
            'items' => $this->order_option_variations,
            'created_at' => $this->created_at->toCookieString(),
        ];
    }
}
