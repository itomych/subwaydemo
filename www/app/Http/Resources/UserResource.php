<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'code' => $this->code,
            'orders' => $this->orders()->without(['order_option_variations'])->get(),
            'roles' => $this->roles->map(function ($role) {return $role->name;}),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
