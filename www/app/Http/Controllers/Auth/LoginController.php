<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginWithCodeRequest;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    /**
     * @var UserService
     */
    private $userService;

    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest')->except('logout');
        $this->userService = $userService;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $token = $this->guard()->attempt($this->credentials($request));

        if ($token) {
            $this->guard()->setToken($token);

            return true;
        }

        return false;
    }

    public function loginWithCode(LoginWithCodeRequest $request)
    {
        $user = $this->userService->findByCode($request->input('code'));

        $loginRequest = new Request([
            'email' => $user->email,
            'password' => $user->code
        ]);

        $token = $this->guard()->attempt($this->credentials($loginRequest));

        if ($token) {
            $this->guard()->setToken($token);

            $expiration = $this->guard()->getPayload()->get('exp');

            return [
                'token' => $token,
                'token_type' => 'bearer',
                'expires_in' => $expiration - time(),
            ];
        }

        return response()->json('Unauthorized.', HttpResponse::HTTP_UNAUTHORIZED);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param Request $request
     * @return array
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        $token = (string) $this->guard()->getToken();
        $expiration = $this->guard()->getPayload()->get('exp');

        return [
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $expiration - time(),
        ];
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();
    }
}
