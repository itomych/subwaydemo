<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginWithCodeRequest;
use App\Http\Resources\User;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class SetPasswordController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest');
        $this->userService = $userService;
    }

    /**
     * @param LoginWithCodeRequest $request
     * @return JsonResponse
     */
    public function setPassword(LoginWithCodeRequest $request)
    {
        $user = $this->userService->findByFilters($request);

        $user = $this->userService->update($user->first(), $request->toArray());

        return (new User($user))->response()->setStatusCode(200);
    }
}
