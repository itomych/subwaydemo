<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\FilterUsersRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Contracts\ModelService;
use App\Http\Resources\UserCollectionResource;
use App\Services\UserService;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * @var ModelService
     */
    private $userService;

    /**
     * UsersController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Creates the user
     *
     * @param CreateUserRequest $request
     * @return JsonResponse
     */
    public function create(CreateUserRequest $request): JsonResponse
    {
        $user = $this->userService->make($request->input());

        return (new UserResource($user))
            ->response()
            ->setStatusCode(201);
    }

    /**
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $userCollection = $this->userService->getAll();

        return (new UserCollectionResource($userCollection))
            ->response()
            ->setStatusCode(200);
    }

    /**
     * @param FilterUsersRequest $request
     * @return Collection
     */
    public function filter(FilterUsersRequest $request)
    {
        return $this->userService->findByFilters($request);
    }

    /**
     * @param User $user
     * @return array
     * @throws Exception
     */
    public function delete(User $user)
    {
        $this->userService->destroy($user);
        return response(['message' => 'Deleted.'], 204);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return JsonResponse
     */
    public function generateNewCode(Request $request, User $user): JsonResponse
    {
        $user = $this->userService->generateNewCode($user);

        return (new UserResource($user))
            ->response()
            ->setStatusCode(200);
    }

    public function update(UpdateUserRequest $request, User $user): JsonResponse
    {
        $user = $this->userService->update($user, $request->toArray());

        return (new UserResource($user))
            ->response()
            ->setStatusCode(200);
    }
}
