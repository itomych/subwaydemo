<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionVariationCreateRequest;
use App\Http\Requests\OptionVariationUpdateAvailabilityRequest;
use App\Http\Requests\OrderCreateRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Http\Resources\OptionVariationResource;
use App\Http\Resources\OrderCollectionResource;
use App\Http\Resources\OrderResource;
use App\OptionVariation;
use App\Order;
use App\Services\OptionVariationService;
use App\Services\OrderService;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * OptionVariationController constructor.
     * @param OrderService $orderService
     */
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param OrderCreateRequest $request
     * @return JsonResponse
     */
    public function create(OrderCreateRequest $request): JsonResponse
    {
        $order = $this->orderService->make($request->toArray());

        return (new OrderResource($order))->response();
    }

    /**
     * @return JsonResponse
     */
    public function getUserOrders(): JsonResponse
    {
        $orders = $this->orderService->findByUser(User::find(Auth::id()));

        return (new OrderCollectionResource($orders))->response();
    }

    /**
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $orders = $this->orderService->getAll();

        return (new OrderCollectionResource($orders))->response();
    }

    /**
     * @param Order $order
     * @param OrderUpdateRequest $request
     * @return JsonResponse
     */
    public function update(Order $order, OrderUpdateRequest $request): JsonResponse
    {
        $order = $this->orderService->update($order, $request->toArray());

        return (new OrderResource($order))->response();
    }
}
