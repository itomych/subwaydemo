<?php

namespace App\Http\Controllers;

use App\Http\Requests\OptionVariationCreateRequest;
use App\Http\Requests\OptionVariationUpdateAvailabilityRequest;
use App\Http\Requests\OptionVariationUpdateRequest;
use App\Http\Resources\OptionVariationResource;
use App\OptionVariation;
use App\Services\OptionVariationService;
use Exception;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class OptionVariationController extends Controller
{
    /**
     * @var OptionVariationService
     */
    private $variationService;

    /**
     * OptionVariationController constructor.
     * @param OptionVariationService $variationService
     */
    public function __construct(OptionVariationService $variationService)
    {
        $this->variationService = $variationService;
    }

    /**
     * @param OptionVariationUpdateAvailabilityRequest $request
     * @param OptionVariation $optionVariation
     * @return JsonResponse
     */
    public function updateAvailability(
        OptionVariationUpdateAvailabilityRequest $request,
        OptionVariation $optionVariation
    ): JsonResponse
    {
        $variation = $this->variationService->update($optionVariation, $request->toArray());

        return (new OptionVariationResource($variation))->response();
    }

    /**
     * @param OptionVariationCreateRequest $request
     * @return JsonResponse
     */
    public function create(OptionVariationCreateRequest $request)
    {
        $variation = $this->variationService->make($request->toArray());

        return (new OptionVariationResource($variation))->response();
    }

    /**
     * @param OptionVariationUpdateRequest $request
     * @param OptionVariation $optionVariation
     * @return JsonResponse|null
     */
    public function update(OptionVariationUpdateRequest $request, OptionVariation $optionVariation): ?JsonResponse
    {
        $variation = $this->variationService->update($optionVariation, $request->toArray());

        return (new OptionVariationResource($variation))->response();
    }

    /**
     * @param OptionVariation $optionVariation
     * @return ResponseFactory|Response
     * @throws Exception
     */
    public function destroy(OptionVariation $optionVariation)
    {

        $this->variationService->destroy($optionVariation);

        return response(['message' => 'Deleted.'], 204);
    }
}
