<?php

namespace App\Http\Controllers;

use App\Http\Resources\OptionCollectionResource;
use App\Http\Resources\OptionResource;
use App\Services\OptionService;
use Illuminate\Http\JsonResponse;

class OptionController extends Controller
{
    /**
     * @var OptionService
     */
    private $optionService;

    /**
     * OptionVariationController constructor.
     * @param OptionService $optionService
     */
    public function __construct(OptionService $optionService)
    {
        $this->optionService = $optionService;
    }

    /**
     * Retrieve all Options
     *
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $options = $this->optionService->getAll();

        return (new OptionCollectionResource($options))
            ->response();
    }

    /**
     * Get single Option by slug
     *
     * @param string $slug
     * @return JsonResponse
     */
    public function getBySlug(string $slug): JsonResponse
    {
        $option = $this->optionService->findBySlug($slug);

        return (new OptionResource($option))
            ->response();
    }
}
