<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !!Auth::id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bread' => 'required',
            'bread_size' => 'required',
            'taste' => 'required',
            'vegetable' => 'required',
            'sauce' => 'required',
            'extra' => 'required',
            'should_be_baked' => 'required',
        ];
    }
}
