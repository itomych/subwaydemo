<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !!Auth::id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bread' => 'int',
            'bread_size' => 'int',
            'taste' => 'int',
            'vegetable' => 'array',
            'sauce' => 'int',
            'extra' => 'int',
            'should_be_baked' => 'boolean',
            'rate' => 'regex:/^\d+(\.\d{1,2})?$/',
        ];
    }
}
