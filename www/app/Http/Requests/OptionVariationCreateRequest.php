<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OptionVariationCreateRequest extends AdminRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return parent::authorize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'option_id' => 'int|required',
            'name' => 'string|required',
            'slug' => 'string|required'
        ];
    }
}
