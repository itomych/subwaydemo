<?php

namespace App\Http\Requests;

use App\Contracts\Requestable;
use Illuminate\Foundation\Http\FormRequest;

class FilterUsersRequest extends AdminRequest implements Requestable
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return parent::authorize();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
