<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AdminRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::user()->roles->where('name', env('ROLE_ADMIN'))->count();
    }
}
