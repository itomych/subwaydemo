<?php

namespace App\Http\Requests;

use App\Contracts\Requestable;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class LoginWithCodeRequest extends FormRequest implements Requestable
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (request('code')) {
            return !!User::where('code', request('code'))->first();
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required'
        ];
    }
}
