<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;

/**
 * Role model
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Role extends Model
{
    /**
     * Casts the attributes
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    protected $fillable = [
        'name', 'description'
    ];

    protected $guarded = [];
}
