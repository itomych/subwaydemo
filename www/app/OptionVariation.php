<?php

namespace App;

use App\Support\Database\CacheQueryBuilder;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * OptionVariation Model
 *
 * @property int $option_id
 * @property string $name
 * @property string $slug
 * @property boolean $available
 * @property DateTime $created_at
 * @property DateTime $updated_at
*/
class OptionVariation extends Model
{
    use CacheQueryBuilder;
    /**
     * Casts the attributes
     *
     * @var array
    */
    protected $casts = [
        'option_id' => 'int',
        'name' => 'string',
        'slug' => 'string',
        'available' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * Set attributes that can be filled
     *
     * @var array
    */
    protected $fillable = [
        'option_id',
        'name',
        'slug',
        'available'
    ];

    protected $guarded = [];

    /**
     * Get related Option model
     *
     * @return BelongsTo
    */
    public function option()
    {
        return $this->belongsTo(Option::class);
    }
}
