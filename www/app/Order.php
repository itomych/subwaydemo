<?php

namespace App;

use App\Support\Database\CacheQueryBuilder;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Order model
 *
 * @property integer $user_id
 * @property object $order_contents
 * @property boolean $status
 * @property float $rate
 * @property DateTime $created_at
 * @property DateTime $updated_at
 */
class Order extends Model
{
    use CacheQueryBuilder;
    /**
     * Casts the attributes
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'order_contents' => 'object',
        'status' => 'boolean',
        'rate' => 'float',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'should_be_baked', 'status', 'rate', 'user_id'
    ];

    protected $guarded = [];

    protected $with = ['order_option_variations'];

    /**
     * Get related User model
     *
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order_option_variations()
    {
        return $this->belongsToMany(OptionVariation::class)->with(['option']);
    }
}
