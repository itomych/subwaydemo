<?php

namespace App;

use App\Support\Database\CacheQueryBuilder;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Option model
 *
 * @property string $name
 * @property HasMany $variations
 * @property DateTime $created_at
 * @property DateTime $updated_at
*/
class Option extends Model
{
    use CacheQueryBuilder;

    /**
     * Cast the attributes
     *
     * @var array
    */
    protected $casts = [
        'name' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    protected $guarded = [];

    /**
     * Get related OptionVariations
     *
     * @return HasMany
    */
    public function variations()
    {
        return $this->hasMany(OptionVariation::class);
    }
}
