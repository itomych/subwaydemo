<?php

namespace App\Services;

use App\Contracts\ModelService;
use App\Contracts\Requestable;
use App\Option;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class OptionService implements ModelService
{

    /**
     * @param array $data
     * @return Model|null
     */
    public function make(array $data): ?Option
    {
        return Option::query()->create($data);
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Option::with(['variations'])->get();
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function findById(int $id): ?Option
    {
        return Option::query()->with(['variations'])->whereKey($id)->first();
    }

    /**
     * @param string $slug
     * @return Model|null
     */
    public function findBySlug(string $slug): ?Option
    {
        return Option::query()->with(['variations'])->where('slug', $slug)->first();
    }

    /**
     * @param Model $option
     * @throws Exception
     */
    public function destroy(Model $option): void
    {
        $option->delete();
    }

    /**
     * @param Requestable $filters
     * @return Collection
     */
    public function findByFilters(Requestable $filters): Collection
    {
        // TODO: Implement findByFilters() method.
    }
}
