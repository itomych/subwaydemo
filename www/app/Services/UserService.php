<?php

namespace App\Services;

use App\Contracts\Requestable;
use App\Contracts\ModelService;
use App\Notifications\Invite;
use App\Notifications\ResendCode;
use App\Notifications\YouAreDeleted;
use App\Role;
use App\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserService implements ModelService
{
    /**
     * @var CodeGeneratorService
     */
    private $codeGeneratorService;

    /**
     * UserService constructor.
     * @param CodeGeneratorService $codeGeneratorService
     */
    public function __construct(CodeGeneratorService $codeGeneratorService)
    {
        $this->codeGeneratorService = $codeGeneratorService;
    }

    /**
     * @param array $data
     * @return Model|null
     */
    public function make(array $data): ?Model
    {
        $data['code'] = $this->codeGeneratorService->generate();

        $data['password'] = password_hash((!empty($data['password']) ? $data['password'] : $data['code']), CRYPT_BLOWFISH);

        $role = Role::query()->where('name', env('ROLE_USER'))->first();

        $user = User::query()->create($data);

        $user->roles()->save($role)->make();

        /** @noinspection PhpUndefinedMethodInspection */
//        $user->notify(new Invite());

        $user->load(['orders']);

        return $user;
    }

    /**
     * @param User $user
     * @param array $data
     * @return Model|null
     */
    public function update(User $user, array $data): ?Model
    {
        if (!empty($data['password'])) {
            $data['password'] = password_hash($data['password'], CRYPT_BLOWFISH);
        }

        $data = array_merge($user->toArray(), $data);

        $user->update($data);

        $user = $user->refresh();

        $user->load(['orders']);

        return $user;
    }

    public function generateNewCode(User $user): ?Model
    {
        $user->code = $this->codeGeneratorService->generate();
        $user->password = password_hash($user->code, CRYPT_BLOWFISH);

        $user->save();

        /** @noinspection PhpUndefinedMethodInspection */
        $user->notify(new ResendCode());
        $user->load(['orders']);

        return $user->refresh();
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return User::with(['orders'])->get();
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function findById(int $id): ?User
    {
        return User::query()->with(['orders'])->whereKey($id)->first();
    }

    /**
     * @param string $code
     * @return Model|null
     */
    public function findByCode(string $code): ?User
    {
        return User::with(['orders'])->where('code', $code)->first();
    }

    /**
     * @param Requestable $filters
     * @return Collection
     */
    public function findByFilters(Requestable $filters): Collection
    {
        $users = User::query();

        if ($filters['name']) {
            $users->where('name', $filters['name']);
        }

        if ($filters['email']) {
            $users->where('email', $filters['email']);
        }

        if ($filters['code']) {
            $users->where('code', $filters['code']);
        }

        return $users->with(['orders'])->get();
    }

    /**
     * @param Model $user
     * @throws Exception
     */
    public function destroy(Model $user): void
    {
        /** @noinspection PhpUndefinedMethodInspection */
//        $user->notify(new YouAreDeleted());
        $user->delete();
    }
}
