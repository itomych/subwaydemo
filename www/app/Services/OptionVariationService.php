<?php

namespace App\Services;

use App\Contracts\ModelService;
use App\Contracts\Requestable;
use App\OptionVariation;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class OptionVariationService implements ModelService
{
    /**
     * @param array $data
     * @return Model|null
     */
    public function make(array $data): ?OptionVariation
    {
        $optionVariation = OptionVariation::query()->create($data);

        $optionVariation->load(['option']);

        return $optionVariation;
    }

    /**
     * Update OptionVariation
     *
     * @param OptionVariation $optionVariation
     * @param array $data
     * @return OptionVariation|null
     */
    public function update(OptionVariation $optionVariation, array $data): ?OptionVariation
    {
        $data = array_merge($optionVariation->toArray(), $data);

        $optionVariation->update($data);
        $optionVariation->load(['option']);

        return $optionVariation;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return OptionVariation::with(['option'])->get();
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function findById(int $id): ?OptionVariation
    {
        return OptionVariation::query()->with(['option'])->whereKey($id)->first();
    }

    /**
     * @param string $slug
     * @return Model|null
     */
    public function findBySlug(string $slug): ?OptionVariation
    {
        return OptionVariation::query()->with(['option'])->where('slug', $slug)->first();
    }

    /**
     * @param Model $optionVariation
     * @throws Exception
     */
    public function destroy(Model $optionVariation): void
    {
        $optionVariation->delete();
    }

    /**
     * @param Requestable $filters
     * @return Collection
     */
    public function findByFilters(Requestable $filters): Collection
    {
        // TODO: Implement findByFilters() method.
    }
}
