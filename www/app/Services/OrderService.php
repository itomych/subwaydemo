<?php


namespace App\Services;


use App\Contracts\ModelService;
use App\Contracts\Requestable;
use App\OptionVariation;
use App\Order;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class OrderService implements ModelService
{
    const OPTION_VARIATIONS = [
        'bread',
        'bread_size',
        'vegetable',
        'taste',
        'sauce',
        'extra'
    ];

    /**
     * @param array $data
     * @return Model|null
     */
    public function make(array $data): ?Order
    {
        $order = Order::query()->create([
            'user_id' => Auth::id(),
            'status' => 1,
            'should_be_baked' => (int) $data['should_be_baked'],
            'rate' => 0
        ]);

        $optionVariations = OptionVariation::all();

        foreach (self::OPTION_VARIATIONS as $optionVariationKey) {
            if ($optionVariationKey === 'vegetable') {
                foreach ($data[$optionVariationKey] as $vegetableKey) {
                    $optionVariation = $optionVariations->where('id', $vegetableKey)->first();
                    $order->order_option_variations()->save($optionVariation)->make();
                }
            } else {
                $optionVariation = $optionVariations->where('id', $data[$optionVariationKey])->first();

                if (!$optionVariation)
                    var_dump($data[$optionVariationKey]);

                $order->order_option_variations()->save($optionVariation)->make();
            }
        }

        $order->load(['order_option_variations', 'order_option_variations.option']);

        return $order;
    }

    /**
     * @param Order $order
     * @param array $data
     * @return Model|null
     */
    public function update(Order $order, array $data): ?Model
    {
        $data = array_merge($order->toArray(), $data);

        $order->update($data);

        $order = $order->refresh();

        $order->load(['user', 'order_option_variations', 'order_option_variations.option']);

        return $order;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Order::all();
    }

    /**
     * @param int $id
     * @return Model|null
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * @param User $user
     * @return Collection
     */
    public function findByUser(User $user): ?Collection
    {
        return $user->orders()->get();
    }

    /**
     * @param Requestable $filters
     * @return Collection
     */
    public function findByFilters(\App\Contracts\Requestable $filters): Collection
    {
        // TODO: Implement findByFilters() method.
    }

    /**
     * @param Model $model
     */
    public function destroy(Model $model): void
    {
        // TODO: Implement destroy() method.
    }
}
