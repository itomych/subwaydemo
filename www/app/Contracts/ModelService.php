<?php


namespace App\Contracts;

use App\Contracts\Requestable;
use App\Contracts\Servicable;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface ModelService extends Servicable
{
    /**
     * @param array $data
     * @return Model|null
     */
    public function make(array $data);

    /**
     * @return Collection
    */
    public function getAll(): Collection;

    /**
     * @param int $id
     * @return Model|null
     */
    public function findById(int $id);


    /**
     * @param Requestable $filters
     * @return Collection
     */
    public function findByFilters(Requestable $filters): Collection;

    /**
     * @param Model $model
     */
    public function destroy(Model $model): void;
}
