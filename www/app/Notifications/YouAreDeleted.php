<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class YouAreDeleted extends Notification
{
    public function via()
    {
        return 'mail';
    }

    /**
     * Build the mail representation of the notification.
     *
     * @return MailMessage
     */
    public function toMail()
    {
        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You are deleted from Subway Order.');
    }
}
