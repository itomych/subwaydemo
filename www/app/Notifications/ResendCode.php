<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResendCode extends Notification
{
    public function via()
    {
        return 'mail';
    }
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You are receiving this email because you requested to regenerate auth code.')
            ->action('Go To Subway', url(config('app.url').'/?code=' . $notifiable->code))
            ->line('If you dont request it, no further action is required.');
    }
}
