<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Invite extends Notification
{
    public function via()
    {
        return 'mail';
    }
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Hello!')
            ->line('You are receiving this email because you invited to Subway Order.')
            ->action('Go To Subway', url(config('app.url').'/?code=' . $notifiable->code))
            ->line('If you dont want to become a member, no further action is required.');
    }
}
