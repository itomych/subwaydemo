import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import {
  faUser,
  faLock,
  faSignOutAlt,
  faCog,
  faList,
  faUserFriends,
  faBreadSlice,
  faBacon,
  faHamburger,
  faCarrot,
  faHome,
  faTachometerAlt,
  faWineBottle
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faHome,
  faUser,
  faLock,
  faSignOutAlt,
  faCog,
  faList,
  faUserFriends,
  faBreadSlice,
  faBacon,
  faHamburger,
  faCarrot,
  faTachometerAlt,
  faWineBottle
)

Vue.component('fa', FontAwesomeIcon)
