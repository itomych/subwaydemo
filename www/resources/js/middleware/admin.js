import store from '~/store'

export default (to, from, next) => {
  if (!store.getters['auth/user'].roles.includes('ADMIN')) {
    next({ name: 'meal' })
  } else {
    next()
  }
}
