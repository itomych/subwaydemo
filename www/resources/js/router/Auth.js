const Login = () => import('~/pages/auth/login').then(m => m.default || m)

export default [
  {path: '/login', name: 'login', component: Login}
]
