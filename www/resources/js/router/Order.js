const OrdersIndex = () => import('~/pages/orders').then(m => m.default || m)
const OrdersUser = () => import('~/pages/ordersUser').then(m => m.default || m)

export default [
  {path: '/orders', name: 'orders', component: OrdersIndex},
  {path: '/my-orders', name: 'orders-user', component: OrdersUser}
]
