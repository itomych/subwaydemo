const VegetableIndex = () => import('~/pages/vegetables').then(m => m.default || m)

export default [
  {path: '/vegetable', name: 'vegetable', component: VegetableIndex}
]
