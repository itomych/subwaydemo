const TasteIndex = () => import('../pages/tastes').then(m => m.default || m)

export default [
  {path: '/taste', name: 'taste', component: TasteIndex}
]
