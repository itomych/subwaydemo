const BreadsIndex = () => import('~/pages/breads').then(m => m.default || m)

export default [
  {path: '/bread', name: 'bread', component: BreadsIndex}
]
