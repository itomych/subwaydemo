const UsersIndex = () => import('~/pages/users').then(m => m.default || m)

export default [
  {path: '/users', name: 'users', component: UsersIndex}
]
