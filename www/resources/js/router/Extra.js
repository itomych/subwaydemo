const ExtrasIndex = () => import('~/pages/extras').then(m => m.default || m)

export default [
  {path: '/extra', name: 'extra', component: ExtrasIndex}
]
