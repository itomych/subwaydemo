const SauceIndex = () => import('~/pages/sauces').then(m => m.default || m)

export default [
  {path: '/sauce', name: 'sauce', component: SauceIndex}
]
