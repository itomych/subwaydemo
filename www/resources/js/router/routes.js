const Home = () => import('~/pages/home').then(m => m.default || m)
const HomeUser = () => import('~/pages/homeUser').then(m => m.default || m)
const NotFound = () => import('~/pages/errors/404').then(m => m.default || m)

import AuthRouter from './Auth'
import OrderRouter from './Order'
import BreadRouter from './Bread'
import VegetableRouter from './Vegetable'
import ExtraRouter from './Extra'
import TasteRouter from './Taste'
import UserRouter from './User'
import SaucesRouter from './Sauces'

export default [
  {path: '', redirect: {name: 'home'}},
  {path: '/', redirect: {name: 'home'}},
  {path: '/home', name: 'home', component: Home},
  {path: '/meal', name: 'meal', component: HomeUser},

  ...AuthRouter,
  ...OrderRouter,
  ...BreadRouter,
  ...ExtraRouter,
  ...TasteRouter,
  ...VegetableRouter,
  ...UserRouter,
  ...SaucesRouter,

  {path: '*', component: NotFound}
]
