import axios from 'axios'
import {cloneDeep, clone} from 'lodash'
import swal from "sweetalert2";

export const state = {
  list: [],
  optionVariantModel: {},
  emptyOptionVariantModel: {
    name: '',
    slug: '',
    available: false
  }
}

export const getters = {
  optionVariantModel: state => clone(state.optionVariantModel),
  emptyOptionVariantModel: state => clone(state.emptyOptionVariantModel),
  list: state => state.list
}

export const mutations = {
  SET_OPTION_VARIANT_MODEL: (state, payload) => {
    state.optionVariantModel = cloneDeep(payload)
  },

  SET_LIST: (state, payload) => {
    state.list = cloneDeep(Object.values(payload))
  },

  ADD_TO_LIST(state, payload) {
    state.list.push(cloneDeep(payload));
  },

  UPDATE_IN_LIST(state, payload) {
    state.list = [
      ...state.list.filter(i => i.id !== payload.id),
      cloneDeep(payload)
    ]
  },

  REMOVE_FROM_LIST(state, payload) {
    state.list = state.list.filter(i => i.id !== payload.id)
  },

  SET_VARIATION(state, variation) {
    state.list.forEach(option => {
      if (option.id === variation.option_id) {
        option.variations = [
          ...option.variations.filter(v => v.id !== variation.id),
          variation
        ]
      }
    })
  },

  REMOVE_VARIATION(state, variation) {
    state.list.forEach(option => {
      if (option.id === variation.option_id) {
        option.variations = [
          ...option.variations.filter(v => v.id !== variation.id),
        ]
      }
    })
  }
}

export const actions = {
  async fetchAll({commit}) {
    try {
      const {data} = await axios.get('/api/options');

      commit('SET_LIST', data.data)
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  },

  async updateVariationAvailability({commit}, variation) {
    try {
      const {data} = await axios.patch(`/api/option-variation/${variation.id}/availability`, variation);

      commit('SET_VARIATION', data.data)
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  },

  async update({commit}, {id, payload}) {
    try {
      const {data} = await axios.put(`/api/option-variation/${id}`, payload);

      commit('SET_VARIATION', data.data)
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  },

  async destroy({commit}, variation) {
    try {
      await axios.delete(`/api/option-variation/${variation.id}`);

      commit('REMOVE_VARIATION', variation)
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  },

  async create({commit}, payload) {
    try {
      const {data} = await axios.post('/api/option-variation/', payload);

      commit('SET_VARIATION', data.data);
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  }
}
