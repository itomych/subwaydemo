import axios from 'axios'
import swal from 'sweetalert2'
import {cloneDeep} from "lodash";

export const state = {
  order: {
    bread: null,
    bread_size: null,
    extra: null,
    taste: null,
    sauce: null,
    vegetable: [],
    should_be_baked: false
  },
  emptyOrder: {
    bread: null,
    bread_size: null,
    extra: null,
    taste: null,
    sauce: null,
    vegetable: [],
    should_be_baked: false
  },

  list: []
}

export const getters = {
  model: state => state.order,
  emptyModel: state => state.emptyOrder,
  list: state => state.list
}

export const mutations = {
  SET_ORDER_OPTION: (state, {type, value}) => {
    state.order[type] = cloneDeep(value)
  },

  SET_ORDER_MODEL: (state, model) => {
    state.order = cloneDeep(model)
  },

  SET_LIST(state, payload) {
    state.list = cloneDeep(payload);
  },

  ADD_TO_LIST(state, payload) {
    state.list.push(cloneDeep(payload));
  },

  UPDATE_IN_LIST(state, payload) {
    state.list = [
      ...state.list.filter(i => i.id !== payload.id),
      cloneDeep(payload)
    ]
  },
}

export const actions = {
  async fetchUserOrders({commit}) {
    const {data} = await axios.get('/api/orders/user');

    commit("SET_LIST", data.data);
  },
  async fetchAll({commit}) {
    const {data} = await axios.get('/api/orders');

    commit("SET_LIST", data.data);
  },

  async setRates({commit}, order) {
    try {
      const {data} = await axios.patch(`/api/order/${order.id}`, {rate: order.rate})

      commit("UPDATE_IN_LIST", data.data);
    } catch (e) {

    }
  },

  async send({commit, state}) {
    try {
      const {data} = await axios.post('/api/order', state.order);

      commit('ADD_TO_LIST', data.data)

      await swal.fire(
        'Good job!',
        'Order created!',
        'success'
      );
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  }
}
