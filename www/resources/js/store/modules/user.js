import axios from 'axios'
import {cloneDeep} from 'lodash'
import swal from "sweetalert2";

export const state = {
  list: [],
  emptyModel: {
    code: '',
    email: '',
    name: '',
    orders: 0
  },
  model: {},
  dataTableHeaders: [
    {
      text: 'Name',
      value: 'name'
    },
    {
      text: 'Email',
      value: 'email'
    },
    {
      text: 'Orders',
      value: 'orders'
    },
    {
      text: 'Link',
      value: '',
      sortable: false
    },
    {
      text: 'Actions',
      value: '',
      align: 'center',
      sortable: false
    }
  ]
}

export const getters = {
  list: state => state.list,
  model: state => state.model,
  emptyModel: state => state.emptyModel,
  headers: state => state.dataTableHeaders,
}

export const mutations = {
  SET_LIST(state, payload) {
    if (Array.isArray(payload)) {
      state.list = payload
    }
  },

  SET_MODEL(state, payload) {
    state.model = cloneDeep(payload)
  },

  ADD_TO_LIST(state, payload) {
    state.list.push(cloneDeep(payload));
  },

  UPDATE_IN_LIST(state, payload) {
    state.list = [
      ...state.list.filter(i => i.id !== payload.id),
      cloneDeep(payload)
    ]
  },

  REMOVE_FROM_LIST(state, payload) {
    state.list = state.list.filter(i => i.id !== payload.id)
  }
}

export const actions = {
  async fetchAll({commit}) {
    try {
      const {data} = await axios.get('/api/users');

      commit('SET_LIST', data.data)
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems happen when users loading!',
        'error'
      );
    }
  },

  async destroyUser({commit}, payload) {
    try {
      await axios.delete(`/api/user/${payload.id}`);

      commit('REMOVE_FROM_LIST', payload);
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  },

  async create({commit}, payload) {
    try {
      const {data} = await axios.post('/api/user', payload);

      commit('ADD_TO_LIST', data.data);
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  },

  async update({state, commit}, {id, payload}) {
    try {
      const {data} = await axios.put(`/api/user/${id}`, payload);

      commit('UPDATE_IN_LIST', data.data);
    } catch (e) {
      await swal.fire(
        'Oops!',
        'Some problems on server!',
        'error'
      );
    }
  }
}
