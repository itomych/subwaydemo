export const state = {
  drawer: true
}

export const getters = {
  drawer: state => state.drawer
}

export const mutations = {
  SET_DRAWER(state, payload) {
    state.drawer = payload
  }
}

export const actions = {

}
