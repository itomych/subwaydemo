<?php

namespace Tests;

use Facebook\WebDriver\Remote\WebDriverBrowserType;
use Facebook\WebDriver\WebDriverPlatform;
use Laravel\Dusk\Page;
use Laravel\Dusk\Browser;
use Laravel\Dusk\TestCase as BaseTestCase;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Illuminate\Foundation\Testing\DatabaseMigrations;

Browser::macro('assertPageIs', function ($page) {
    if (!$page instanceof Page) {
        $page = new $page;
    }

    return $this->assertPathIs($page->url());
});

abstract class DuskTestCase extends BaseTestCase
{
    use DatabaseMigrations;
    use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions)->addArguments([
            '--disable-gpu',
            '--headless',
            '--window-size=1920,1080'
        ]);

        $capabilities = new DesiredCapabilities([
            ChromeOptions::CAPABILITY => $options
        ]);

        $capabilities
            ->setPlatform(WebDriverPlatform::ANY)
            ->setBrowserName(WebDriverBrowserType::CHROME);

        return RemoteWebDriver::create('http://192.168.99.100:4444/wd/hub', $capabilities);
    }
}
