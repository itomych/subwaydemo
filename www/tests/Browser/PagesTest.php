<?php

namespace Tests\Browser;

use App\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Tests\Browser\Pages\{Home, Login, Orders, Breads, Extras, Tastes, Vegetables, Users};
use Throwable;

class PagesTest extends DuskTestCase
{
    public function setUp()
    {
        parent::setup();

        static::closeAll();
    }

    public function login()
    {
        $user = factory(User::class)->create();

        try {
            $this->browse(function (Browser $browser) use ($user) {
                $browser->visit(new Login)
                    ->submit($user->email, 'secret')
                    ->assertPageIs(Home::class);
            });
        } catch (Throwable $e) {
        }
    }

    /** @test
     * @throws Throwable
     */
    public function orders_test()
    {
        $this->login();

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new Orders)
                ->assertSee(Orders::title());
        });
    }

    /** @test
     * @throws Throwable
     */
    public function breads_test()
    {
        $this->login();

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new Breads())
                ->assertSee(Breads::title());
        });
    }

    /** @test
     * @throws Throwable
     */
    public function extras_test()
    {
        $this->login();

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new Extras())
                ->assertSee(Extras::title());
        });
    }

    /** @test
     * @throws Throwable
     */
    public function tastes_test()
    {
        $this->login();

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new Tastes())
                ->assertSee(Tastes::title());
        });
    }

    /** @test
     * @throws Throwable
     */
    public function vegetables_test()
    {
        $this->login();

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new Vegetables())
                ->assertSee(Vegetables::title());
        });
    }

    /** @test
     * @throws Throwable
     */
    public function users_test()
    {
        $this->login();

        $this->browse(function (Browser $browser) {
            $browser
                ->visit(new Users())
                ->assertSee(Users::title());
        });
    }
}
