<?php

namespace Tests\Browser\Pages;

class Breads extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/breads';
    }

    public static function title()
    {
        return 'Breads';
    }
}
