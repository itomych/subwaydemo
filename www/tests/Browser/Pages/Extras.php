<?php

namespace Tests\Browser\Pages;

class Extras extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/extras';
    }

    public static function title()
    {
        return 'Extras';
    }
}
