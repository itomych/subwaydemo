<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;

class Home extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/home';
    }

    /**
     * Click on the log out link.
     *
     * @param  Browser $browser
     * @return void
     */
    public function clickLogout($browser)
    {
        $browser->press('#logout')
            ->pause(300);
    }
}
