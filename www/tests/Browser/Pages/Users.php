<?php

namespace Tests\Browser\Pages;

class Users extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/users';
    }

    public static function title()
    {
        return 'Users';
    }
}
