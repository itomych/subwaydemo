<?php

namespace Tests\Browser\Pages;

class Vegetables extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/vegetables';
    }

    public static function title()
    {
        return 'Vegetables';
    }
}
