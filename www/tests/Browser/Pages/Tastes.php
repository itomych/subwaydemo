<?php

namespace Tests\Browser\Pages;

class Tastes extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/tastes';
    }

    public static function title()
    {
        return 'Tastes';
    }
}
