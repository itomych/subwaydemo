<?php

namespace Tests\Browser\Pages;

class Orders extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/orders';
    }

    /**
     * Get the title for the page
     */
    public static function title()
    {
        return 'Orders';
    }
}
