<?php

use App\Http\Resources\AuthUserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return (new AuthUserResource($request->user()))->response();
    });

    Route::get('/users', 'UsersController@list');
    Route::post('/user', 'UsersController@create');
    Route::patch('/user/{user}/code', 'UsersController@generateNewCode');
    Route::put('/user/{user}', 'UsersController@update');
    Route::delete('/user/{user}', 'UsersController@delete');

    Route::get('/options', 'OptionController@getAll');

    Route::patch(
        '/option-variation/{optionVariation}/availability',
        'OptionVariationController@updateAvailability'
    );
    Route::post('/option-variation', 'OptionVariationController@create');
    Route::put('/option-variation/{optionVariation}', 'OptionVariationController@update');
    Route::delete('/option-variation/{optionVariation}', 'OptionVariationController@destroy');

    Route::post('/order', 'OrderController@create');
    Route::get('/orders/user', 'OrderController@getUserOrders');
    Route::get('/orders', 'OrderController@getAll');
    Route::patch('/order/{order}', 'OrderController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('login-with-code', 'Auth\LoginController@loginWithCode');
});
